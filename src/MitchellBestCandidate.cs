﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    public class MitchellBestCandidate
    {
        private int _candidatesPerRound;
        private float _minDistance;

        public List<Vector3> _points = new List<Vector3>();
        private Vector3 _currenPoint;
        private int _currentPointIndex = 0;

        private static Random _rnd = new Random(115);

        private Vector3[] _candidatesBunch;
        private float _radius;

        public MitchellBestCandidate(int candidatesPerRound, float minimalDistance, float r)
        {
            _candidatesPerRound = candidatesPerRound;
            _minDistance = minimalDistance;
            _radius = r;

            _candidatesBunch = new Vector3[_candidatesPerRound];

            ComputeBestPoints();
        }

        private void ComputeBestPoints()
        {
            _currenPoint = GetRandomPoint(_radius);
            _points.Add(_currenPoint);
            _currentPointIndex++;
            int failedPick = 0;
            do
            {
                SetCandidates();
                Vector3 bestCandidate = PickUpCandidate();
                if (bestCandidate != new Vector3(0))
                {
                    _points.Add(bestCandidate);
                    _currenPoint = bestCandidate;
                }
                else
                {
                    failedPick++;
                }
                _currentPointIndex++;
            } while (failedPick < 100);
        }

        private void SetCandidates()
        {
            for (int i = 0; i < _candidatesPerRound; i++)
            {
                _candidatesBunch[i] = GetRandomPoint(_radius);
            }
        }

        private Vector3 PickUpCandidate()
        {
            Dictionary<Vector3, double> candidatesMinimalDistance = new Dictionary<Vector3, double>();
            foreach (Vector3 candidate in _candidatesBunch)
            {
                double minimalDistanceToCloud = MinimalDistanceToCloudToCandidate(candidate);

                // if the distance is above or equal the minDistance add it
                if (minimalDistanceToCloud >= _minDistance)
                {
                    candidatesMinimalDistance.Add(candidate, minimalDistanceToCloud); // In the java example he used Double.valueOf(minimalDistanceToCloud)
                }
            }

            Vector3 bestCandidate = GetFarthestPoint(candidatesMinimalDistance);
            return bestCandidate;
        }

        private double MinimalDistanceToCloudToCandidate(Vector3 candidate)
        {
            double minimalDistance = 0;

            foreach (Vector3 p in _points)
            {
                double d = (candidate - p).Length; // distance between candidate and point
                if (minimalDistance == 0 || d < minimalDistance)
                {
                    minimalDistance = d;
                }
            }

            return minimalDistance;
        }

        private Vector3 GetFarthestPoint(Dictionary<Vector3, double> candidatesMinimalDistance)
        {
            Vector3 result = new Vector3();
            double maxDistance = 0;
            foreach (Vector3 p in candidatesMinimalDistance.Keys)
            {
                if (maxDistance < candidatesMinimalDistance[p])
                {
                    result = p;
                    maxDistance = candidatesMinimalDistance[p];
                }
            }

            return result;
        }

        private static Vector3 GetRandomPoint(float r)
        {
            double u = _rnd.NextDouble() * 2 * r - r;
            

            Vector3 position;
            double azi = 2 * Math.PI * u; // azimuthal angle


            position.X = (float)(Math.Sqrt(Math.Pow(r, 2) - Math.Pow(u, 2)) * Math.Cos(azi * 10));
            position.Y = (float)(Math.Sqrt(Math.Pow(r, 2) - Math.Pow(u, 2)) * Math.Sin(azi * 10));
            position.Z = (float)u;

            return position;
        }
    }
}
