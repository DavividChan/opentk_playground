﻿using EarcutNet;
using Mapbox.Vector.Tile;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES30;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static EarcutNet.Earcut;

namespace OpenTK_Playground.src
{
    class PolygonTriangulation : GameWindow
    {
        bool _isSetuped;

        Program _baseProgram;

        List<Polygon> _polygons;
        int _polygonsBufferSize;
        int _polygonsIndiceCount;

        Camera _cam;
        Matrix4 _projection;
        private Vector2 _lastMousePos = new Vector2();

        uint _vbo, _ebo;

        public PolygonTriangulation()
            : base(
                width: 1200,
                height: 900,
                mode: new GraphicsMode(32, 24, 0, 4),
                title: "Polygon Triangulation"
            )
        { }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            //GL.Enable(EnableCap.CullFace);

            // Program
            string baseVert = OpenGLUtil.LoadShader("base.vert");
            string baseFrag = OpenGLUtil.LoadShader("base.frag");
            _baseProgram = new Program(baseVert, baseFrag);

            StreamReader stream = new StreamReader("../../Assets/MVT/21495.mvt");

            List<VectorTileLayer> layers = VectorTileParser.Parse(stream.BaseStream);

            foreach (var layer in layers)
            {
                if (layer.Name == "water")
                {
                    foreach (var feature in layer.VectorTileFeatures)
                    {
                        List<List<Coordinate>> geom = feature.Geometry;

                        _polygons = Earcut.CalculatePolygonsWithHoles(geom);

                        for (int p = 0; p < _polygons.Count; p++)
                        {
                            _polygons[p].indices = Earcut.Tessellate(_polygons[p].vertices, _polygons[p].holes);
                            _polygonsBufferSize += _polygons[p].BufferSize;
                            _polygonsIndiceCount += _polygons[p].indices.Count;
                        }
                    }
                }
            }

            _cam = new Camera();
            _cam.Position = new Vector3(0.5f, 0.5f, 1.5f);

            _projection = Matrix4.CreatePerspectiveFieldOfView(
                fovy: (float)(45.0f * (Math.PI / 180.0f)),
                aspect: (float)Width / (float)Height, // needs to be casted to float or else it crashes
                zNear: 0.05f,
                zFar: 200.0f
            );

            GL.GenBuffers(1, out _vbo);
            GL.GenBuffers(1, out _ebo);

            // VBO
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, _polygonsBufferSize, (IntPtr)0, BufferUsageHint.DynamicDraw);

            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr) 0, _polygons[0].BufferSize, _polygons[0].vertices.ToArray());
            int offset = 0;
            for (int i = 1; i < _polygons.Count; i++)
            {
                offset += _polygons[i - 1].BufferSize;
                GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr) offset, _polygons[i].BufferSize, _polygons[i].vertices.ToArray());
            }

            // EBO
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, _polygonsIndiceCount * sizeof(int), (IntPtr)0, BufferUsageHint.DynamicDraw);

            GL.BufferSubData(BufferTarget.ElementArrayBuffer, (IntPtr)0, _polygons[0].indices.Count * sizeof(int), _polygons[0].indices.ToArray());
            offset = 0;
            for (int i = 1; i < _polygons.Count; i++)
            {
                offset += _polygons[i - 1].indices.Count * sizeof(int);
                GL.BufferSubData(BufferTarget.ElementArrayBuffer, (IntPtr) offset, _polygons[i].indices.Count * sizeof(int), _polygons[i].indices.ToArray());
            }

            GL.UseProgram(_baseProgram.Handle);
            GL.Uniform3(GL.GetUniformLocation(_baseProgram.Handle, "fragmentColor"), new Vector3(0.2f, 0.3f, 1.0f));
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            //ProcessInput();

            Matrix4 mvp = Matrix4.Mult(_cam.GetViewMatrix(), _projection);
            GL.UniformMatrix4(GL.GetUniformLocation(_baseProgram.Handle, "MVP"), false, ref mvp);
        }

        private void OnRender()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.EnableVertexAttribArray(0);

            GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Double, false, 2 * sizeof(double), (IntPtr)0);
            GL.DrawElements(PrimitiveType.Triangles, _polygons[0].indices.Count, DrawElementsType.UnsignedInt, (IntPtr)0);

            int verticesOffset = 0;
            int indiceOffset = 0;
            for (int i = 1; i < _polygons.Count; i++)
            {
                verticesOffset += _polygons[i - 1].BufferSize;
                indiceOffset += _polygons[i - 1].indices.Count * sizeof(int);

                GL.VertexAttribPointer(0, 2, VertexAttribPointerType.Double, false, 2 * sizeof(double), (IntPtr) verticesOffset);
                GL.DrawElements(PrimitiveType.Triangles, _polygons[i].indices.Count, DrawElementsType.UnsignedInt, (IntPtr) indiceOffset);
            }

            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            GL.Viewport(ClientRectangle);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            if (_isSetuped)
            {
                OnRender();
            }
            else
            {
                GL.Enable(EnableCap.DepthTest);
                _isSetuped = true;
            }
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);
            _lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
        }


        private void ProcessInput()
        {
            if (Keyboard.GetState().IsKeyDown(Key.W))
            {
                _cam.Move(0f, 0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.S))
            {
                _cam.Move(0f, -0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.A))
            {
                _cam.Move(-0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.D))
            {
                _cam.Move(0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Q))
            {
                _cam.Move(0f, 0f, 0.1f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.E))
            {
                _cam.Move(0f, 0f, -0.1f);
            }

            if (Focused)
            {
                Vector2 delta = _lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                _lastMousePos += delta;

                _cam.AddRotation(delta.X, delta.Y);
                _lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Escape))
            {
                Exit();
            }
        }
    }
}
