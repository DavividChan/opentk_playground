﻿using OpenTK_Playground.src.Containers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Text;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    class EntryPoint
    {
        static void Main(string[] args)
        {
            using (PolygonTriangulation game = new PolygonTriangulation())
            {
                game.Run(60.0, 00.0);
            }
        }
    }
}
