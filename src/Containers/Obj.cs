﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    public class Obj
    {
        public List<Vector3> Positions = new List<Vector3>();
        public List<Vector3> Normals = new List<Vector3>();
        public List<Vector2> TextureCoordinates = new List<Vector2>();

        public List<Face> Faces = new List<Face>();
    }

    public class Face
    {
        public FaceVertex[] Vertices = new FaceVertex[3];

        public Face(FaceVertex[] vertices)
        {
            Vertices = vertices;
        }
    }

    public class FaceVertex
    {
        public int Position;
        public int Texcoord;
        public int Normal;
        
        public FaceVertex(int vert = 0, int tex = 0, int norm = 0)
        {
            Position = vert;
            Texcoord = tex;
            Normal = norm;
        }
    }
}
