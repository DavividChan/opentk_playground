﻿using OpenTK;
using OpenTK.Graphics.ES30;
using System.Collections.Generic;
using System.Diagnostics;

namespace OpenTK_Playground.src
{
    class Program
    {
        public int Handle;
        public Dictionary<string, int> Uniforms;

        public Program(string vertexSource, string fragmentSource)
        {
            Handle = CreateProgram(vertexSource, fragmentSource);
        }

        private int CreateProgram(string vertexSource, string fragmentSource)
        {
            int vs = GL.CreateShader(ShaderType.VertexShader);
            int fs = GL.CreateShader(ShaderType.FragmentShader);

            int statusCode;

            // Vertex Shader
            GL.ShaderSource(vs, vertexSource);
            GL.CompileShader(vs);

            GL.GetShader(vs, ShaderParameter.CompileStatus, out statusCode);

            if (statusCode == 0)
            {
                string errorMessage = GL.GetShaderInfoLog(vs);
                Debug.WriteLine("Error compiling vertex shader: ");
                Debug.WriteLine(vertexSource);
                Debug.WriteLine(errorMessage);
            }

            // Fragment Shader
            GL.ShaderSource(fs, fragmentSource);
            GL.CompileShader(fs);

            GL.GetShader(fs, ShaderParameter.CompileStatus, out statusCode);

            if (statusCode == 0)
            {
                string errorMessage = GL.GetShaderInfoLog(fs);
                Debug.WriteLine("Error compiling fragment shader: ");
                Debug.WriteLine(fragmentSource);
                Debug.WriteLine(errorMessage);
            }

            // Program
            int program = GL.CreateProgram();

            GL.AttachShader(program, vs);
            GL.AttachShader(program, fs);
            GL.LinkProgram(program);

            // Cleanup
            GL.DetachShader(program, vs);
            GL.DetachShader(program, fs);

            GL.DeleteShader(vs);
            GL.DeleteShader(fs);

            OpenGLUtil.CheckError();
            
            return program;
        }

        public void SetUniform(string name, bool value)
        {
            int uniformLocation = GL.GetUniformLocation(Handle, name);
            GL.Uniform1(uniformLocation, value ? 1 : 0);
        }

        public void SetUniform(string name, float value)
        {
            int uniformLocation = GL.GetUniformLocation(Handle, name);
            GL.Uniform1(uniformLocation, value);
        }

        public void SetUniform(string name, int value)
        {
            int uniformLocation = GL.GetUniformLocation(Handle, name);
            GL.Uniform1(uniformLocation, value);
        }

        public void SetUniform(string name, Vector3 value)
        {
            int uniformLocation = GL.GetUniformLocation(Handle, name);
            GL.Uniform3(uniformLocation, ref value);
        }

        public void SetUniform(string name, bool transpose, Matrix4 value)
        {
            int uniformLocation = GL.GetUniformLocation(Handle, name);
            GL.UniformMatrix4(uniformLocation, transpose, ref value);
        }
    }


}
