﻿using OpenTK;
using OpenTK.Graphics.ES30;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    // Mesh Information
    public class Mesh
    {
        public BufferBlock VertexBufferBlock = new BufferBlock();
        public BufferBlock IndexBufferBlock = new BufferBlock();

        public VertexData[] VertexData;
        public int[] Indices;
    }

    public struct VertexData
    {
        public float xPos, yPos, zPos;
        public float xTex, yTex;
        public float xNor, yNor, zNor;
    }
}

