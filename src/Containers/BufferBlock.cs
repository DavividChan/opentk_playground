﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    public class BufferBlock
    {
        public int BufferHandle;
        public int Offset;
        public int Size;
    }
}
