﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    // A very naive partial K-D Tree implementation with K = 3 for Points.
    public class ThreeDTree
    {
        private Node _root;

        public void Insert(Vector3 coordinate)
        {
            _root = Node.Insert(coordinate, _root, 0);
        }

        public Vector3 FindNearest(Vector3 candidate, out double bestDistance)
        {
            bestDistance = double.MaxValue;
            Node best = Node.FindNearest(candidate, _root, 0, null, ref bestDistance);
            return best.Coordinate;
        }

        public IEnumerable<Vector3> GetPoints()
        {
            if (_root != null)
                return _root.GetPoints();
            return Enumerable.Empty<Vector3>();
        }

        private class Node
        {
            private Node _left;
            private Node _right;

            public readonly Vector3 Coordinate;

            public Node(Vector3 coord)
            {
                Coordinate = coord;
            }

            public IEnumerable<Vector3> GetPoints()
            {
                if (_left != null)
                {
                    foreach (var pt in _left.GetPoints())
                        yield return pt;
                }
                yield return Coordinate;

                if (_right != null)
                {
                    foreach (var pt in _right.GetPoints())
                        yield return pt;
                }
            }

            // recursive insert (non-balanced).
            public static Node Insert(Vector3 coord, Node root, int level)
            {
                if (root == null)
                    return new Node(coord);

                int compareResult;
                switch (level % 3)
                {
                    case 1:
                        compareResult = coord.Y.CompareTo(root.Coordinate.Y);
                        break;
                    case 2:
                        compareResult = coord.Z.CompareTo(root.Coordinate.Z);
                        break;
                    default:
                        compareResult = coord.X.CompareTo(root.Coordinate.X);
                        break;
                }

                if (compareResult > 0)
                    root._right = Insert(coord, root._right, level + 1);
                else
                    root._left = Insert(coord, root._left, level + 1);
                return root;
            }

            public static Node FindNearest(Vector3 coord, Node root, int level, Node best, ref double bestDistance)
            {
                if (root == null)
                    return best;

                float axis_dif;
                switch (level % 3)
                {
                    case 1:
                        axis_dif = coord.Y - root.Coordinate.Y;
                        break;
                    case 2:
                        axis_dif = coord.Z - root.Coordinate.Z;
                        break;
                    default:
                        axis_dif = coord.X - root.Coordinate.X;
                        break;
                }

                // recurse near & maybe far as well
                Node near = axis_dif <= 0.0d ? root._left : root._right;
                best = FindNearest(coord, near, level + 1, best, ref bestDistance);
                if (axis_dif * axis_dif < bestDistance)
                {
                    Node far = axis_dif <= 0.0d ? root._right : root._left;
                    best = FindNearest(coord, far, level + 1, best, ref bestDistance);
                }

                // do we beat the old best?
                float dist = (root.Coordinate - coord).Length;

                if (dist < bestDistance)
                {
                    bestDistance = dist;
                    return root;
                }

                return best;
            }
        }
    }

    // Mitchell Best Candidate algorithm, using the K-D Tree.
    public class MitchellBestCandidate1
    {
        private readonly Random _rnd;
        private readonly ThreeDTree _tree = new ThreeDTree();

        private readonly int _maxCandidates;
        private static float _radius;
        private static float _minDist;
        private static float _maxFailed; // How often can a randomised point be not generated
        
        public MitchellBestCandidate1(int maxCandidatesPerRound, float radius, float minDist, float maxFailed = 100)
        { 
            _maxCandidates = maxCandidatesPerRound;
            _rnd = new Random();
            _radius = radius;
            _minDist = minDist;
            _maxFailed = maxFailed;
        }

        public IEnumerable<Vector3> CurrentPoints
        {
            get { return _tree.GetPoints(); }
        }

        public void Generate()
        {
            _tree.Insert(GetRandomPoint(_rnd));

            int failedPoint = 0;
            do
            {
                double bestDistance = double.MinValue;
                Vector3 bestCandidate = new Vector3();
                for (var ci = 0; ci < _maxCandidates; ci++)
                {
                    double distance;
                    Vector3 candidate = GetRandomPoint(_rnd);
                    Vector3 nearest = _tree.FindNearest(candidate, out distance);

                    // if the nearest is to close to candidate then don't set bestCandidate
                    if ((nearest - candidate).Length >= _minDist)
                    {
                        if (distance > bestDistance)
                        {
                            bestDistance = distance;
                            bestCandidate = candidate;
                        }
                    }
                }
                if (bestCandidate != new Vector3(0))
                {
                    _tree.Insert(bestCandidate);
                }
                else
                {
                    failedPoint++;
                }

            } while (failedPoint < _maxFailed);
        }

        private static Vector3 GetRandomPoint(Random rnd)
        {
            double u = rnd.NextDouble() * 2 * _radius - _radius;

            Vector3 position;
            double azi = 2 * Math.PI * u; // azimuthal angle

            position.X = (float)(Math.Sqrt(Math.Pow(_radius, 2) - Math.Pow(u, 2)) * Math.Cos(azi * 20));
            position.Y = (float)(Math.Sqrt(Math.Pow(_radius, 2) - Math.Pow(u, 2)) * Math.Sin(azi * 20));
            position.Z = (float)u;

            return position;
        }
    }
}
