﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES30;
using OpenTK.Input;
using OpenTK_Playground.src.Containers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace OpenTK_Playground.src
{
    class MainWindow : GameWindow
    {
        // On first time where OnRenderFrame get's called, updateFrame will not be called before that
        // So on the first frame we only Setup our scene
        private bool _isSetuped;

        // Program
        private Program _textureProgram;
        private Program _earthProgram;
        private int _vbo;
        private int _ebo;

        // sphere Mesh
        private Mesh _sphere;
        private Obj _sphereObj;

        // Light
        private Light _light;

        // Textures
        private Dictionary<string, int> _textures;

        // Earth
        private Material _earthMat;
        private Matrix4 _earthModel;

        // Sun
        private Matrix4 _sunModel;

        // Camera
        private Matrix4 _projection;
        private Camera _cam;
        private Vector2 _lastMousePos = new Vector2();

        public MainWindow()
            : base(
                width: 800,
                height: 600,
                mode: new GraphicsMode(32, 24, 0, 4),
                title: "OpenTK Playground"
            )
        { }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Programs
            string textureVert = OpenGLUtil.LoadShader("texture.vert");
            string textureFrag = OpenGLUtil.LoadShader("texture.frag");
            _textureProgram = new Program(textureVert, textureFrag);

            string earthVert = OpenGLUtil.LoadShader("phong.vert");
            string earthFrag = OpenGLUtil.LoadShader("earth.frag");
            _earthProgram = new Program(earthVert, earthFrag);

            _earthMat = new Material(new Vector3(0.6f), new Vector3(0.6f), new Vector3(0.6f), 10);

            GL.GenBuffers(1, out _vbo);
            GL.GenBuffers(1, out _ebo);

            // sphere Mesh
            _sphere = new Mesh();
            _sphereObj = MeshLoader.LoadMeshFromFile("earth.obj");
            _sphere.VertexData = MeshLoader.GetVertexDataNotEfficient(_sphereObj);
            _sphere.Indices = MeshLoader.GetIndicesNotEfficient(_sphereObj);

            _sphere.VertexBufferBlock.Size = _sphere.VertexData.Length * sizeof(float) * 8;
            _sphere.VertexBufferBlock.Offset = 0;
            _sphere.IndexBufferBlock.Size = _sphere.Indices.Length * sizeof(int);
            _sphere.IndexBufferBlock.Offset = 0;

            // Light
            _light = new Light(new Vector3(0.0f, 3.0f, 3.0f), new Vector3(1.0f, 1.0f, 1.0f));

            // Textures
            _textures = new Dictionary<string, int>();
            _textures.Add("earth_daymap", OpenGLUtil.LoadImage("earth_daymap.jpg")); //"earth_daymap.jpg"
            _textures.Add("earth_nightmap", OpenGLUtil.LoadImage("earth_nightmap.jpg"));
            _textures.Add("earth_clouds", OpenGLUtil.LoadImage("earth_clouds.jpg"));
            _textures.Add("earth_normal_map", OpenGLUtil.LoadImage("earth_normal_map.png"));
            _textures.Add("earth_specular_map", OpenGLUtil.LoadImage("earth_specular_map_8k.png"));
            _textures.Add("stars_milky_way", OpenGLUtil.LoadImage("stars_milky_way.png"));
            _textures.Add("sun", OpenGLUtil.LoadImage("sun.png"));

            // Camera
            _projection = Matrix4.CreatePerspectiveFieldOfView(
                fovy: (float)(45.0f * (Math.PI / 180.0f)),
                aspect: (float)Width / (float)Height, // needs to be casted to float or else it crashes
                zNear: 0.05f,
                zFar: 200.0f
            );

            _cam = new Camera();
            _cam.Position = new Vector3(0.0f, 0.0f, 5.0f);

            _lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            CursorVisible = false;

            int totalVertexBufferSize = _sphere.VertexBufferBlock.Size;
            int totalIndexBufferSize = _sphere.IndexBufferBlock.Size;

            // VBO
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, totalVertexBufferSize, IntPtr.Zero, BufferUsageHint.DynamicDraw);
            GL.BufferSubData(BufferTarget.ArrayBuffer, (IntPtr)_sphere.VertexBufferBlock.Offset, _sphere.VertexBufferBlock.Size, _sphere.VertexData);

            // EBO
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, totalIndexBufferSize, IntPtr.Zero, BufferUsageHint.DynamicDraw);
            GL.BufferSubData(BufferTarget.ElementArrayBuffer, (IntPtr)_sphere.IndexBufferBlock.Offset, _sphere.IndexBufferBlock.Size, _sphere.Indices);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);
            GL.EnableVertexAttribArray(2);

            // Sphere AttribPointers
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 8 * sizeof(float), IntPtr.Zero);
            GL.VertexAttribPointer(1, 2, VertexAttribPointerType.Float, true, 8 * sizeof(float), 3 * sizeof(float));
            GL.VertexAttribPointer(2, 3, VertexAttribPointerType.Float, true, 8 * sizeof(float), 5 * sizeof(float));
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            Title = "OpenTK Playground ms: " + RenderTime;

            ProcessInput();
        }

        private void OnRender()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            // Earth Program
            GL.UseProgram(_earthProgram.Handle);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);
            GL.EnableVertexAttribArray(2);

            // Textures
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, _textures["earth_daymap"]);
            _earthProgram.SetUniform("t_earthDay", 0);

            GL.ActiveTexture(TextureUnit.Texture1);
            GL.BindTexture(TextureTarget.Texture2D, _textures["earth_nightmap"]);
            _earthProgram.SetUniform("t_earthNight", 1);

            GL.ActiveTexture(TextureUnit.Texture2);
            GL.BindTexture(TextureTarget.Texture2D, _textures["earth_specular_map"]);
            _earthProgram.SetUniform("t_specular", 2);

            // Earth
            _earthModel = Matrix4.Identity;
            Matrix4 view = _cam.GetViewMatrix();
            Matrix4 sphereMVP = Matrix4.Mult(_earthModel, Matrix4.Mult(view, _projection));
            _earthProgram.SetUniform("modelview", false, sphereMVP);
            _earthProgram.SetUniform("view", false, view);
            _earthProgram.SetUniform("model", false,  _earthModel);
            _earthProgram.SetUniform("light.position",  _light.Position);
            _earthProgram.SetUniform("light.color",  _light.Color);
            _earthProgram.SetUniform("light.ambientIntensity", _light.AmbientIntensity);
            _earthProgram.SetUniform("light.diffuseIntensity", _light.DiffuseIntensity);
            _earthProgram.SetUniform("material.ambient", _earthMat.AmbientColor);
            _earthProgram.SetUniform("material.diffuse", _earthMat.DiffuseColor);
            _earthProgram.SetUniform("material.specular",  _earthMat.SpecularColor);
            _earthProgram.SetUniform("material.specExponent", _earthMat.SpecularExponent);

            GL.DrawElements(PrimitiveType.Triangles, _sphere.Indices.Length, DrawElementsType.UnsignedInt, (IntPtr)0);

            // Sun
            GL.UseProgram(_textureProgram.Handle);

            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, _textures["sun"]);
            _textureProgram.SetUniform("t_texture", 0);

            _sunModel = Matrix4.CreateTranslation(_light.Position);
            sphereMVP = Matrix4.Mult(_sunModel, Matrix4.Mult(view, _projection));
            _textureProgram.SetUniform("MVP", false, sphereMVP);

            GL.DrawElements(PrimitiveType.Triangles, _sphere.Indices.Length, DrawElementsType.UnsignedInt, (IntPtr)0);

            // Stars
            GL.ActiveTexture(TextureUnit.Texture0);
            GL.BindTexture(TextureTarget.Texture2D, _textures["stars_milky_way"]);
            _textureProgram.SetUniform("t_texture", 0);

            Matrix4 starSphereModel = Matrix4.CreateScale(new Vector3(100.0f, 100.0f, 100.0f));
            sphereMVP = Matrix4.Mult(starSphereModel, Matrix4.Mult(view, _projection));
            _textureProgram.SetUniform("MVP", false, sphereMVP);

            GL.DrawElements(PrimitiveType.Triangles, _sphere.Indices.Length, DrawElementsType.UnsignedInt, (IntPtr)0);

            // Cleanup
            GL.DisableVertexAttribArray(0);
            GL.DisableVertexAttribArray(1);
            GL.DisableVertexAttribArray(2);

            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            GL.Viewport(ClientRectangle);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            if (_isSetuped)
            {
                OnRender();
            }
            else
            {
                GL.ClearColor(Color4.MidnightBlue);
                GL.Enable(EnableCap.DepthTest);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                _isSetuped = true;
            }
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);

            _lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
        }

        // TODO lagere diese Methode aus
        private void ProcessInput()
        {
            if (Keyboard.GetState().IsKeyDown(Key.W))
            {
                _cam.Move(0f, 0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.S))
            {
                _cam.Move(0f, -0.1f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.A))
            {
                _cam.Move(-0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.D))
            {
                _cam.Move(0.1f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Q))
            {
                _cam.Move(0f, 0f, 0.1f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.E))
            {
                _cam.Move(0f, 0f, -0.1f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Up))
            {
                _light.Position = RotateAroundPointX(Vector3.Zero, _light.Position, (float)RenderTime);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Down))
            {
                _light.Position = RotateAroundPointX(Vector3.Zero, _light.Position, -(float)RenderTime);
            }

            if (Focused)
            {
                Vector2 delta = _lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                _lastMousePos += delta;

                _cam.AddRotation(delta.X, delta.Y);
                _lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Escape))
            {
                Exit();
            }
        }

        private Vector3 RotateAroundPointX(Vector3 pointToRotateAround, Vector3 rotatedPosition, float angle)
        {
            // https://math.stackexchange.com/questions/2628960/rotate-around-a-specific-point-instead-of-0-0-0
            Matrix4 R = Matrix4.CreateRotationX(angle);
            Vector4 u = new Vector4(rotatedPosition, 1);
            Vector4 o = new Vector4(Vector3.Zero, 0);
            Vector4 OP = new Vector4(pointToRotateAround, 1) - o;
            Vector4 result = R * (u - OP) + OP;

            return new Vector3(result.X, result.Y, result.Z);
        }
    }
}
