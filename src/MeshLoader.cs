﻿using OpenTK;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    class MeshLoader
    {
        public static Obj LoadMeshFromFile(string filename)
        {
            string folder = "../../Assets/Models/";

            Obj obj = new Obj();

            try
            {
                using (StreamReader reader = new StreamReader(new FileStream(folder + filename, FileMode.Open, FileAccess.Read)))
                {
                    obj = LoadFromString(reader.ReadToEnd());
                }
            }
            catch (FileNotFoundException e)
            {
                Console.WriteLine("File not found: {0}", filename);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error loading file: {0}", filename);
            }

            return obj;
        }

        private static Obj LoadFromString(string objSource)
        {
            Obj obj = new Obj();

            // Seperate lines from the file
            List<string> lines = new List<string>(objSource.Split('\n'));

            // Read file line by line
            foreach (string line in lines)
            {
                if (line.StartsWith("v ")) // Vertex definition
                {
                    // Cut off beginning of line
                    string temp = line.Replace('.', ',').Substring(2);

                    Vector3 vec = new Vector3();

                    if (temp.Trim().Count((char c) => c == ' ') == 2) // Check if there's enough elements for a vertex
                    {
                        string[] vertparts = temp.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        // Attempt to parse each part of the vertice
                        bool success = float.TryParse(vertparts[0], out vec.X);
                        success |= float.TryParse(vertparts[1], out vec.Y);
                        success |= float.TryParse(vertparts[2], out vec.Z);

                        // If any of the parses failed, report the error
                        if (!success)
                        {
                            Console.WriteLine("Error parsing vertex: {0}", line);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error parsing vertex: {0}", line);
                    }

                    obj.Positions.Add(vec);
                }
                else if (line.StartsWith("vt ")) // Texture coordinate
                {
                    // Cut off beginning of line
                    string temp = line.Replace('.', ',').Substring(2);

                    Vector2 vec = new Vector2();

                    if (temp.Trim().Count((char c) => c == ' ') > 0) // Check if there's enough elements for a vertex
                    {
                        string[] texcoordparts = temp.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        // Attempt to parse each part of the vertice
                        bool success = float.TryParse(texcoordparts[0], out vec.X);
                        success |= float.TryParse(texcoordparts[1], out vec.Y);

                        // If any of the parses failed, report the error
                        if (!success)
                        {
                            Console.WriteLine("Error parsing texture coordinate: {0}", line);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error parsing texture coordinate: {0}", line);
                    }

                    obj.TextureCoordinates.Add(vec);
                }
                else if (line.StartsWith("vn ")) // Normal vector
                {
                    // Cut off beginning of line
                    string temp = line.Replace('.', ',').Substring(2);

                    Vector3 vec = new Vector3();

                    if (temp.Trim().Count((char c) => c == ' ') == 2) // Check if there's enough elements for a normal
                    {
                        string[] vertparts = temp.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        // Attempt to parse each part of the vertice
                        bool success = float.TryParse(vertparts[0], out vec.X);
                        success |= float.TryParse(vertparts[1], out vec.Y);
                        success |= float.TryParse(vertparts[2], out vec.Z);

                        // If any of the parses failed, report the error
                        if (!success)
                        {
                            Console.WriteLine("Error parsing normal: {0}", line);
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error parsing normal: {0}", line);
                    }

                    obj.Normals.Add(vec);
                }
                else if (line.StartsWith("f ")) // Face definition
                {
                    // Cut off beginning of line
                    string temp = line.Replace('.', ',').Substring(2);

                    if (temp.Trim().Count((char c) => c == ' ') == 2) // Check if there's enough elements for a face
                    {
                        string[] faceparts = temp.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        int v1, v2, v3;
                        int t1, t2, t3;
                        int n1, n2, n3;

                        // Attempt to parse each part of the face
                        bool success = int.TryParse(faceparts[0].Split('/')[0], out v1);
                        success |= int.TryParse(faceparts[1].Split('/')[0], out v2);
                        success |= int.TryParse(faceparts[2].Split('/')[0], out v3);

                        if (faceparts[0].Count((char c) => c == '/') >= 2)
                        {
                            success |= int.TryParse(faceparts[0].Split('/')[1], out t1);
                            success |= int.TryParse(faceparts[1].Split('/')[1], out t2);
                            success |= int.TryParse(faceparts[2].Split('/')[1], out t3);
                            success |= int.TryParse(faceparts[0].Split('/')[2], out n1);
                            success |= int.TryParse(faceparts[1].Split('/')[2], out n2);
                            success |= int.TryParse(faceparts[2].Split('/')[2], out n3);
                        }
                        else
                        {
                            if (obj.TextureCoordinates.Count > v1 && obj.TextureCoordinates.Count > v2 && obj.TextureCoordinates.Count > v3)
                            {
                                t1 = v1;
                                t2 = v2;
                                t3 = v3;
                            }
                            else
                            {
                                t1 = 0;
                                t2 = 0;
                                t3 = 0;
                            }


                            if (obj.Normals.Count > v1 && obj.Normals.Count > v2 && obj.Normals.Count > v3)
                            {
                                n1 = v1;
                                n2 = v2;
                                n3 = v3;
                            }
                            else
                            {
                                n1 = 0;
                                n2 = 0;
                                n3 = 0;
                            }
                        }


                        // If any of the parses failed, report the error
                        if (!success)
                        {
                            Console.WriteLine("Error parsing face: {0}", line);
                        }
                        else
                        {
                            obj.Faces.Add(new Face(
                                new FaceVertex[]{
                                    new FaceVertex(v1, t1, n1),
                                    new FaceVertex(v2, t2, n2),
                                    new FaceVertex(v3, t3, n3)
                                }
                            ));
                        }
                    }
                    else
                    {
                        Console.WriteLine("Error parsing face: {0}", line);
                    }
                }
            }

            return obj;
        }

        public static VertexData[] GetVertexDataNotEfficient(Obj obj)
        {
            VertexData[] vertexData = new VertexData[obj.Faces.Count * 3];

            for (int i = 0; i < obj.Faces.Count; i++)
            {
                for (int a = 0; a < 3; a++)
                {
                    // Positions
                    vertexData[i * 3 + a].xPos = obj.Positions[obj.Faces[i].Vertices[a].Position - 1].X;
                    vertexData[i * 3 + a].yPos = obj.Positions[obj.Faces[i].Vertices[a].Position - 1].Y;
                    vertexData[i * 3 + a].zPos = obj.Positions[obj.Faces[i].Vertices[a].Position - 1].Z;

                    // TexCoords
                    vertexData[i * 3 + a].xTex = obj.TextureCoordinates[obj.Faces[i].Vertices[a].Texcoord - 1].X;
                    vertexData[i * 3 + a].yTex = obj.TextureCoordinates[obj.Faces[i].Vertices[a].Texcoord - 1].Y;

                    // Normals
                    vertexData[i * 3 + a].xNor = obj.Normals[obj.Faces[i].Vertices[a].Normal - 1].X;
                    vertexData[i * 3 + a].yNor = obj.Normals[obj.Faces[i].Vertices[a].Normal - 1].Y;
                    vertexData[i * 3 + a].zNor = obj.Normals[obj.Faces[i].Vertices[a].Normal - 1].Z;
                }
            }

            return vertexData;
        }

        public static int[] GetIndicesNotEfficient(Obj obj)
        {
            int[] indices = new int[obj.Faces.Count * 3];
            for (int i = 0; i < obj.Faces.Count; i++)
            {
                indices[i * 3] = i * 3;
                indices[i * 3 + 1] = i * 3 + 1;
                indices[i * 3 + 2] = i * 3 + 2;
            }

            return indices;
        }

            public static VertexData[] GetVertexData(Obj obj)
        {
            VertexData[] vertexData = new VertexData[obj.Positions.Count];

            for (int i = 0; i < vertexData.Length; i++)
            {
                // Wenn nicht genügend daten zur verfügung steht, guck in die faces und nimm den wert der zum index correspondiert
                if (i > obj.TextureCoordinates.Count - 1)
                {
                    vertexData[i].xPos = obj.Positions[obj.Faces[i / 3].Vertices[i % 3].Position - 1].X;
                    vertexData[i].yPos = obj.Positions[obj.Faces[i / 3].Vertices[i % 3].Position - 1].Y;
                    vertexData[i].zPos = obj.Positions[obj.Faces[i / 3].Vertices[i % 3].Position - 1].Z;
                }

                else
                {
                    vertexData[i].xPos = obj.Positions[i].X;
                    vertexData[i].yPos = obj.Positions[i].Y;
                    vertexData[i].zPos = obj.Positions[i].Z;
                }

                if (i > obj.TextureCoordinates.Count - 1)
                {
                    vertexData[i].xTex = obj.TextureCoordinates[obj.Faces[i / 3].Vertices[i % 3].Texcoord - 1].X;
                    vertexData[i].yTex = obj.TextureCoordinates[obj.Faces[i / 3].Vertices[i % 3].Texcoord - 1].Y;
                }
                else
                {
                    vertexData[i].xTex = obj.TextureCoordinates[i].X;
                    vertexData[i].yTex = obj.TextureCoordinates[i].Y;
                }

                if (i > obj.Normals.Count - 1)
                {
                    vertexData[i].xNor = obj.Normals[obj.Faces[i / 3].Vertices[i % 3].Normal - 1].X;
                    vertexData[i].yNor = obj.Normals[obj.Faces[i / 3].Vertices[i % 3].Normal - 1].Y;
                    vertexData[i].zNor = obj.Normals[obj.Faces[i / 3].Vertices[i % 3].Normal - 1].Z;
                }
                else
                {
                    vertexData[i].xNor = obj.Normals[i].X;
                    vertexData[i].yNor = obj.Normals[i].Y;
                    vertexData[i].zNor = obj.Normals[i].Z;
                }
            }

            return vertexData;
        }

        // TODO calculate indices the right way
        // This method only works if no vertex lies on another one (with position)
        // What can happen if for example "smothing groups" enabled is.
        public static int[] GetIndices(Obj obj)
        {
            int[] indicesUnpacked = new int[obj.Faces.Count * 3];

            //List<FaceVertex> vertices = new List<FaceVertex>();

            for (int i = 0; i < obj.Faces.Count; i++)
            {
                //vertices.Add(obj.Faces[i].Vertex1);
                //vertices.Add(obj.Faces[i].Vertex2);
                //vertices.Add(obj.Faces[i].Vertex3);

                indicesUnpacked[i * 3] = obj.Faces[i].Vertices[0].Position - 1;
                indicesUnpacked[i * 3 + 1] = obj.Faces[i].Vertices[1].Position - 1;
                indicesUnpacked[i * 3 + 2] = obj.Faces[i].Vertices[2].Position - 1;
            }

            //for (int i = 0; i < vertices.Count; i++)
            //{
            //    int foundIndex = IsVertexMultiple(i, vertices);

            //    if (foundIndex != -1)
            //    {
            //        indicesUnpacked[i] = indicesUnpacked[foundIndex];
            //    }
            //}

            return indicesUnpacked;
        }

        //private static int IsVertexMultiple(int index, List<FaceVertex> vertices)
        //{
        //    for (int i = 0; i < vertices.Count; i++)
        //    {
        //        if (index == i || index < i)
        //        {
        //            continue;
        //        }
        //        if (vertices[index].Position == vertices[i].Position && vertices[index].Texcoord == vertices[i].Texcoord && vertices[index].Normal == vertices[i].Normal)
        //        {
        //            return i;
        //        }
        //    }

        //    return -1;
        //}
    }
}
