﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES30;
using OpenTK.Input;
using OpenTK_Playground.src.Containers;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Timers;

namespace OpenTK_Playground.src
{
    class RandomDotsWindow : GameWindow
    {
        bool _isSetuped;

        // Program
        Program _baseProgram;
        int _vbo;
        int _instancedVBO;
        int _ebo;

        // Plain Mesh
        Mesh _sphere;
        Obj _sphereObj;

        // Points
        Matrix4[] _points;
        Random _rnd = new Random();

        // Camera
        Matrix4 _projection;
        Matrix4 _model;
        Matrix4 _MVP;
        Camera _cam;
        Vector2 _lastMousePos = new Vector2();

        // time
        DateTime startTime;
        DateTime endTime;
        TimeSpan timeSpent;

        // Mitchell Best Candidate
        float radius = 100.0f; // radius = modelScale
        float pointScale = 0.5f;
        float minDist = 1.5f; // jede 5m
        int candidates = 15;
        int maxFailTimes = 1000;
        MitchellBestCandidate1 _mitchellBestCandidate;

        public RandomDotsWindow()
            : base(
                width: 1200,
                height: 900,
                mode: new GraphicsMode(32, 24, 0, 4),
                title: "Random Dots"
            )
        { }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Camera
            _projection = Matrix4.CreatePerspectiveFieldOfView(
                fovy: (float)(45.0f * (Math.PI / 180.0f)),
                aspect: (float)Width / (float)Height, // needs to be casted to float or else it crashes
                zNear: 0.05f,
                zFar: 12000.0f
            );

            _cam = new Camera();
            _cam.Position = new Vector3(0.0f, 0.0f, 150.0f);

            _lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
            CursorVisible = false;

            // Program
            string baseVert = OpenGLUtil.LoadShader("base.vert");
            string baseFrag = OpenGLUtil.LoadShader("base.frag");
            _baseProgram = new Program(baseVert, baseFrag);

            GL.GenBuffers(1, out _vbo);
            GL.GenBuffers(1, out _instancedVBO);
            GL.GenBuffers(1, out _ebo);

            // Plain Mesh
            _sphere = new Mesh();
            _sphereObj = MeshLoader.LoadMeshFromFile("sphere.obj");
            _sphere.VertexData = MeshLoader.GetVertexDataNotEfficient(_sphereObj);
            _sphere.Indices = MeshLoader.GetIndicesNotEfficient(_sphereObj);

            _mitchellBestCandidate = new MitchellBestCandidate1(candidates, radius, minDist, maxFailTimes);

            // Profile time for the Generatiing method
            startTime = DateTime.Now;

            _mitchellBestCandidate.Generate();

            endTime = DateTime.Now;
            timeSpent = endTime - startTime;
            Title = "Random Dots | MBC Generation time spent: " + timeSpent.TotalSeconds + "s";

            List<Vector3> points = new List<Vector3>();
            points.Add(new Vector3(0));
            foreach (Vector3 point in _mitchellBestCandidate.CurrentPoints)
            {
                points.Add(point / pointScale);
            }

            _model = Matrix4.CreateScale(new Vector3(radius));

            _points = new Matrix4[points.Count];
            for (int p = 0; p < points.Count; p++)
            {
                _points[p] = Matrix4.CreateScale(new Vector3(pointScale)) * Matrix4.CreateTranslation(points[p]);
            }

            // VBO
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);
            GL.BufferData(BufferTarget.ArrayBuffer, _sphere.VertexData.Length * sizeof(float) * 8 , _sphere.VertexData, BufferUsageHint.StaticDraw);

            GL.BindBuffer(BufferTarget.ArrayBuffer, _instancedVBO);
            GL.BufferData(BufferTarget.ArrayBuffer, points.Count * sizeof(float) * 3, points.ToArray(), BufferUsageHint.StaticDraw);

            // EBO
            GL.BindBuffer(BufferTarget.ElementArrayBuffer, _ebo);
            GL.BufferData(BufferTarget.ElementArrayBuffer, _sphere.Indices.Length * sizeof(int), _sphere.Indices, BufferUsageHint.StaticDraw);
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);

            ProcessInput();
        }

        private void OnRender()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.UseProgram(_baseProgram.Handle);

            GL.EnableVertexAttribArray(0);
            GL.EnableVertexAttribArray(1);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _vbo);

            // BaseProgram AttribPointers
            GL.VertexAttribPointer(0, 3, VertexAttribPointerType.Float, false, 8 * sizeof(float), IntPtr.Zero);
            GL.BindBuffer(BufferTarget.ArrayBuffer, _instancedVBO);
            GL.VertexAttribPointer(1, 3, VertexAttribPointerType.Float, false, 3 * sizeof(float), (IntPtr)0);

            _MVP = Matrix4.Mult(_model, Matrix4.Mult(_cam.GetViewMatrix(), _projection));
            _baseProgram.SetUniform("MVP", false, _MVP);
            _baseProgram.SetUniform("fragmentColor", new Vector3(0.0f, 0.5f, 0.0f));

            GL.VertexAttribDivisor(1, 1);

            GL.DrawElements(PrimitiveType.Triangles, _sphere.Indices.Length, DrawElementsType.UnsignedInt, (IntPtr)0);

            // Multiple dots
            _baseProgram.SetUniform("fragmentColor", new Vector3(1.0f, 1.0f, 1.0f));

            Matrix4 pointModel = Matrix4.Mult(Matrix4.CreateScale(pointScale), Matrix4.Mult(_cam.GetViewMatrix(), _projection));
            _baseProgram.SetUniform("MVP", false, pointModel);

            GL.DrawArraysInstanced(PrimitiveType.Triangles, 0, _sphere.VertexData.Length, _points.Length);

            //for (int i = 0; i < _points.Length; i++)
            //{
            //    Matrix4 MVP = Matrix4.Mult(_points[i], Matrix4.Mult(_cam.GetViewMatrix(), _projection));

            //    _baseProgram.SetUniform("MVP", false, MVP);

            //    GL.DrawArrays(PrimitiveType.Triangles, 0, _sphere.VertexData.Length);
            //    //GL.DrawElements(PrimitiveType.Triangles, _sphere.Indices.Length, DrawElementsType.UnsignedInt, (IntPtr)0);
            //}

            // Cleanup
            GL.DisableVertexAttribArray(0);

            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            GL.Viewport(ClientRectangle);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            if (_isSetuped)
            {
                OnRender();
            }
            else
            {
                //GL.ClearColor(Color4.MidnightBlue);
                GL.Enable(EnableCap.DepthTest);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                _isSetuped = true;
            }
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);

            _lastMousePos = new Vector2(Mouse.GetState().X, Mouse.GetState().Y);
        }

        // TODO lagere diese Methode aus
        private void ProcessInput()
        {
            if (Keyboard.GetState().IsKeyDown(Key.W))
            {
                _cam.Move(0f, 1.0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.S))
            {
                _cam.Move(0f, -1.0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.A))
            {
                _cam.Move(-1.0f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.D))
            {
                _cam.Move(1.0f, 0f, 0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Q))
            {
                _cam.Move(0f, 0f, 1.0f);
            }

            if (Keyboard.GetState().IsKeyDown(Key.E))
            {
                _cam.Move(0f, 0f, -1.0f);
            }

            if (Focused)
            {
                Vector2 delta = _lastMousePos - new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
                _lastMousePos += delta;

                _cam.AddRotation(delta.X, delta.Y);
                _lastMousePos = new Vector2(OpenTK.Input.Mouse.GetState().X, OpenTK.Input.Mouse.GetState().Y);
            }

            if (Keyboard.GetState().IsKeyDown(Key.Escape))
            {
                Exit();
            }
        }
    }
}
