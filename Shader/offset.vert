﻿#version 300 es

in vec3 v_position;
in vec3 v_offset;

uniform mat4 MVP;

void main(void)
{
	gl_Position = MVP * vec4(v_position + v_offset, 1.0);
}