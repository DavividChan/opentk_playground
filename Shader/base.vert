﻿#version 300 es

in vec3 v_position;

uniform mat4 MVP;

void main(void)
{
	gl_Position = MVP * vec4(v_position, 1.0);
}