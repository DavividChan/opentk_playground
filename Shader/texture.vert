﻿#version 300 es

layout(location = 0) in vec3 v_position;
layout(location = 1) in vec2 v_texCoord;

uniform mat4 MVP;

out vec2 texCoord;

void main()
{
	gl_Position = MVP * vec4(v_position, 1.0);	

	texCoord = v_texCoord;
}