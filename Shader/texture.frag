﻿#version 300 es
precision mediump float;

in vec2 texCoord;

uniform sampler2D t_texture;

out vec4 color;

void main()
{
	vec2 flippedCoord = vec2(texCoord.x, 1.0 - texCoord.y);
	vec4 fragColor = texture(t_texture, flippedCoord);

	color = fragColor;
}