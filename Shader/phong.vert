﻿#version 300 es

in vec3 vPosition;
in vec2 texCoord;
in vec3 vNormal;


out vec3 v_norm;
out vec3 v_pos;
out vec2 f_texCoord;

uniform mat4 modelview;
uniform mat4 model;
uniform mat4 view;

void main()
{
	gl_Position = modelview * vec4(vPosition, 1.0);
	f_texCoord = texCoord;

	mat3 normMatrix = transpose(inverse(mat3(model)));
	v_norm = normMatrix * vNormal;
	v_pos = (model * vec4(vPosition, 1.0)).xyz;
}