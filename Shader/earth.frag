﻿#version 300 es
precision mediump float;

struct Light 
{
	vec3 position;
	vec3 color;
	float ambientIntensity;
	float diffuseIntensity;
};

struct Material
{
	vec3 ambient;
	vec3 diffuse;
	vec3 specular;
	float specExponent;
};

in vec3 v_norm;
in vec2 f_texCoord;
in vec3 v_pos;

out vec4 outputColor;

uniform sampler2D t_earthDay;
uniform sampler2D t_earthNight;
uniform sampler2D t_specular;
uniform mat4 view;
uniform Light light;
uniform Material material;

void main()
{
	vec3 n = normalize(v_norm);
	vec3 lightVec = normalize(light.position - v_pos);
	float lambertMaterialDiffuse = max(dot(n, lightVec), 0.0);

	// Colors
	vec2 flippedTexCoord = vec2(f_texCoord.x, 1.0 - f_texCoord.y);

	vec4 dayColor = texture(t_earthDay, flippedTexCoord);

	vec4 nightColor = texture(t_earthNight, flippedTexCoord);

	vec4 texColor = mix(nightColor, dayColor, lambertMaterialDiffuse);

	vec4 lightAmbient = light.ambientIntensity * vec4(light.color, 0.0);
	vec4 lightDiffuse = light.diffuseIntensity * vec4(light.color, 0.0);

	// Ambient
	vec4 ambientColor = texColor * lightAmbient * vec4(material.ambient, 0.0);

	// Diffuse
	vec4 diffuseColor = (lightDiffuse * texColor * vec4(material.diffuse, 0.0)) * lambertMaterialDiffuse;

	// Specular
	vec3 reflectionVec = normalize(reflect(-lightVec, v_norm));
	vec3 viewVec = normalize(vec3(inverse(view) * vec4(0,0,0,1)) - v_pos);
	float materialSpecularReflection = max(dot(v_norm, lightVec), 0.0) * pow(max(dot(reflectionVec, viewVec), 0.0), material.specExponent);

	// Specular map
	materialSpecularReflection = materialSpecularReflection * texture(t_specular, flippedTexCoord);

	vec4 specularColor = vec4(material.specular * light.color, 0.0) * materialSpecularReflection;

	// Result
	outputColor = ambientColor + diffuseColor + specularColor;
	outputColor.a = 1.0;
}
