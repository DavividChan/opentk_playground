﻿using OpenTK_Playground.src;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PolygonTriangulation
{
    class Program
    {
        static void Main(string[] args)
        {
            using (MainWindow game = new MainWindow())
            {
                game.Run(60.0, 00.0);
            }
        }
    }
}
