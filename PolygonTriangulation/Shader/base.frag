﻿#version 300 es
precision mediump float;

out vec4 color;

uniform vec3 fragmentColor;

void main(void)
{
	color = vec4(fragmentColor, 1.0);
}