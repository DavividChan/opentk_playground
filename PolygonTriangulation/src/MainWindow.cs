﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.ES30;
using OpenTK.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices;
using System.Timers;

namespace OpenTK_Playground.src
{
    class MainWindow : GameWindow
    {
        bool _isSetuped;

        Program _baseProgram;

        Vector2[] _polygon;

        public MainWindow()
            : base(
                width: 1200,
                height: 900,
                mode: new GraphicsMode(32, 24, 0, 4),
                title: "Polygon Triangulation"
            )
        { }

        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);

            // Program
            string baseVert = OpenGLUtil.LoadShader("base.vert");
            string baseFrag = OpenGLUtil.LoadShader("base.frag");
            _baseProgram = new Program(baseVert, baseFrag);

            // Polygon
            _polygon = new Vector2[]
            {
                new Vector2(-0.300475f, 0.862924f),
                new Vector2(0.302850f, 1.265013f),
                new Vector2(0.811164f, 1.437337f),
                new Vector2(1.001188f, 1.071802f),
                new Vector2(0.692399f, 0.936031f),
                new Vector2(0.934679f, 0.622715f),
                new Vector2(0.644893f, 0.408616f),
                new Vector2(0.592637f, 0.753264f),
                new Vector2(0.269596f, 0.278068f),
                new Vector2(0.996437f, -0.092689f),
                new Vector2(0.735154f, -0.338120f),
                new Vector2(0.112827f, 0.079634f),
                new Vector2(-0.167458f, 0.330287f),
                new Vector2(0.008314f, 0.664491f),
                new Vector2(0.393112f, 1.040470f),
            };

            List<int> indices = new List<int>();

            for(int i = 0; i < _polygon.Length; i++)
            {
                indices.Add(i);
            }

            List<List<int>> monotone_poly_list = new List<List<int>>();

            bool b_change = false;
            do
            {

                List<int> sorted_vertex_list = new List<int>();

                for (int i = 0, n = indices.Count; i < n; ++i)
                {
                    sorted_vertex_list.Add(i);
                }

                do
                {
                    b_change = false;
                    for (int i = 1, n = sorted_vertex_list.Count; i < n; ++i)
                    {
                        int a = sorted_vertex_list[i - 1];
                        int b = sorted_vertex_list[i];

                        if (_polygon[indices[a]].Y < _polygon[indices[b]].Y)
                        {
                            int safe = sorted_vertex_list[i - 1];
                            sorted_vertex_list[i - 1] = sorted_vertex_list[i];
                            sorted_vertex_list[i] = safe;

                            b_change = true;
                        }
                    }

                } while (b_change);
                // sort vertex indices by the y coordinate
                // (note this is using bubblesort to maintain portability
                // but it should be done using a better sorting method)

                b_change = false;

                for (int i = 0, n = sorted_vertex_list.Count, m = indices.Count; i < n; ++i)
                {
                    int n_ith = sorted_vertex_list[i];
                    Vector2 ith = _polygon[indices[n_ith]];
                    Vector2 prev = _polygon[indices[(n_ith + m - 1) % m]];
                    Vector2 next = _polygon[indices[(n_ith + 1) % m]];
                    // get point in the list, and get it's neighbours
                    // (neighbours are not in sorted list ordering
                    // but in the original polygon order)

                    float sidePrev = Math.Sign(ith.Y - prev.Y);
                    float sideNext = Math.Sign(ith.Y - next.Y);
                    // calculate which side they lie on
                    // (sign function gives -1 for negative and 1 for positive argument)

                    if (sidePrev * sideNext >= 0) // if they are both on the same side
                    {
                        int n_next = -1;
                        if (sidePrev + sideNext > 0)
                        {
                            if (i > 0)
                            {
                                // get the next vertex above it
                                n_next = sorted_vertex_list[i - 1];
                            }
                        }
                        else
                        {
                            if (i + 1 < n)
                            {
                                // get the next vertex below it
                                n_next = sorted_vertex_list[i + 1];
                            }
                        }
                        // this is kind of simplistic, one needs to check if
                        // a line between n_ith and n_next doesn't exit the polygon
                        // (but that doesn't happen in the example)

                        if (n_next != -1)
                        {
                            List<int> poly = new List<int>();
                            List<int> remove_list = new List<int>();

                            int n_last = n_ith;

                            if (n_last > n_next)
                            {
                                int safe = n_last;
                                n_last = n_next;
                                n_next = safe;
                            }

                            int idx = n_next;
                            poly.Add(indices[idx]); // add n_next
                            for (idx = (idx + 1) % m; idx != n_last; idx = (idx + 1) % m)
                            {
                                poly.Add(indices[idx]);
                                // add it to the polygon

                                remove_list.Add(idx);
                                // mark this vertex to be erased from the indices

                            }
                            poly.Add(indices[idx]); // add n_ith
                            // build a new monotone polygon by cutting the original one

                            remove_list.Sort();

                            for (int j = remove_list.Count; j > 0; --j)
                            {
                                int n_which = remove_list[j - 1];
                                // TODO .begin gibt es nicht, kann daher hier falsch sein
                                indices.RemoveAt(n_which);
                            }
                            // sort indices of vertices to be removed and remove them
                            // from the working set (have to do it in reverse order)

                            monotone_poly_list.Add(poly);
                            // add it to the list

                            b_change = true;

                            // the polygon was sliced, restart the algorithm, regenerate sorted list and slice again
                        }
                    }
                }

            } while (b_change);

            if (indices.Count != 0)
            {
                monotone_poly_list.Add(indices);
                // use the remaining vertices (which the algorith was unable to slice) as the last polygon
            }

            //List<List<int>>.Enumerator p_mono_poly = monotone_poly_list.;

            foreach (List<int> p_mono_poly in monotone_poly_list)
            {
                List<int> r_mono_poly = p_mono_poly;

                int n_top = 0;
                for (int i = 0, n = r_mono_poly.Count; i < n; ++i)
                {
                    if (_polygon[r_mono_poly[i]].Y < _polygon[r_mono_poly[n_top]].Y)
                    {
                        n_top = i;
                    }
                }
                // find the top-most point

                
            }
        }

        protected override void OnUpdateFrame(FrameEventArgs e)
        {
            base.OnUpdateFrame(e);
        }

        private void OnRender()
        {
            GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.UseProgram(_baseProgram.Handle);

            SwapBuffers();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            GL.Viewport(ClientRectangle);
        }

        protected override void OnRenderFrame(FrameEventArgs e)
        {
            base.OnRenderFrame(e);

            if (_isSetuped)
            {
                OnRender();
            }
            else
            {
                //GL.ClearColor(Color4.MidnightBlue);
                GL.Enable(EnableCap.DepthTest);
                GL.Enable(EnableCap.Blend);
                GL.BlendFunc(BlendingFactorSrc.SrcAlpha, BlendingFactorDest.OneMinusSrcAlpha);
                _isSetuped = true;
            }
        }

        protected override void OnFocusedChanged(EventArgs e)
        {
            base.OnFocusedChanged(e);
        }
    }
}
