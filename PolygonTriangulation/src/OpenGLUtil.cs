﻿using OpenTK;
using OpenTK.Graphics.ES30;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenTK_Playground.src
{
    class OpenGLUtil
    {
        public static string LoadShader(string filename)
        {
            // C:\Users\David Radobenko\Documents\C-Sharp\OpenTK_Playground\bin\Debug
            string folder = "../../Shader/";
            using (StreamReader sr = new StreamReader(folder + filename))
            {
                return sr.ReadToEnd();
            }
        }

        public static void CheckError()
        {
            ErrorCode ec = GL.GetError();

            while (ec != ErrorCode.NoError)
            {
                Debug.WriteLine("GL Error: " + ec.ToString());
                ec = GL.GetError();
            }
        }

        public static int LoadImage(Bitmap image)
        {
            int texID = GL.GenTexture();

            GL.BindTexture(TextureTarget.Texture2D, texID);
            BitmapData data = image.LockBits(new Rectangle(0, 0, image.Width, image.Height),
                ImageLockMode.ReadOnly, System.Drawing.Imaging.PixelFormat.Format32bppArgb);

            // The newer version doesn't support Bgra
            //GL.TexImage2D(TextureTarget2d.Texture2D, 0, TextureComponentCount.Rgba, data.Width, data.Height, 9, OpenTK.Graphics.ES30.PixelFormat.Rgba, PixelType.UnsignedByte, data.Scan0);
            GL.TexImage2D(All.Texture2D, 0, All.Rgba, data.Width, data.Height, 0, All.BgraExt, All.UnsignedByte, data.Scan0);
            GL.GenerateMipmap(TextureTarget.Texture2D);

            image.UnlockBits(data);

            return texID;
        }

        public static int LoadImage(string filename)
        {
            string folder = "../../Assets/Textures/";
            try
            {
                Bitmap file = new Bitmap(folder + filename);
                return LoadImage(file);
            }
            catch (FileNotFoundException e)
            {
                return -1;
            }
        }
    }
}
